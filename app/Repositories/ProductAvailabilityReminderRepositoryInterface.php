<?php

namespace App\Repositories;


/**
 * Interface ProductRepositoryInterface.
 */
interface ProductAvailabilityReminderRepositoryInterface
{

    public function registerForProductAvailabilityReminder($email, $language, $productSku);

    public function notify($productSku);
}
