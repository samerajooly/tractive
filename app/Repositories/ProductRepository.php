<?php

namespace App\Repositories;

use App\Product;

/**
 * Class ProductRepository.
 */
class ProductRepository implements ProductRepositoryInterface
{


    public function all()
    {
        logger('Get all products');
        return Product::all();
    }

    public function update($id, array $data)
    {
        logger('update product');
        try {
            $product = Product::find($id);
            $product->quantity = $data['quantity'];
            $product->save();
            logger("product updated successfully");
            return ['success' => true];
        } catch (\Exception $e) {
            logger($e);
            return ['success' => false, 'error' => $e->getMessage()];
        }
    }


}
