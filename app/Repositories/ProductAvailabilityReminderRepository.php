<?php

namespace App\Repositories;

use App\Mail\ProductAvailableMail;
use App\Product;
use App\ProductAvailabilityReminder;
use Illuminate\Support\Facades\Mail;

/**
 * Class ProductAvailabilityReminderRepository.
 */
class ProductAvailabilityReminderRepository implements ProductAvailabilityReminderRepositoryInterface
{

    public function registerForProductAvailabilityReminder($email, $language, $productSku)
    {
        logger("registerForProductAvailabilityReminder");
        try {
            $productAvailabilityReminder = new ProductAvailabilityReminder();
            logger("registerForProductAvailabilityReminder2");

            $productAvailabilityReminder->setEmailAddress($email);
            $productAvailabilityReminder->setProductSku($productSku);
            $productAvailabilityReminder->setLanguage($language);
            logger("registerForProductAvailabilityReminder3");


            $productAvailabilityReminder->save();
            logger("everything successful");
            return ['success' => true];
        } catch (\Exception $e) {
            logger($e);
            return ['success' => false, 'error' => $e->getMessage()];
        }
    }

    public function notify($productSku)
    {
        logger("Notifying people of product " . $productSku);
        try {
            $product = Product::query()->where('sku', $productSku)->first();
            $productReminders = ProductAvailabilityReminder::query()->where('product_sku', $productSku)->get();
            foreach ($productReminders as $productReminder) {
                Mail::to($productReminder->email)->send(new ProductAvailableMail($product, $productReminder->language));
            }
            logger('Emails sent successfully');
            ProductAvailabilityReminder::query()->where('product_sku', $productSku)->delete();
            logger('Records deleted successfully');
        } catch (\Exception $e) {
            logger($e);
        }
    }
}
