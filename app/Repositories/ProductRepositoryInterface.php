<?php

namespace App\Repositories;


/**
 * Interface ProductRepositoryInterface.
 */
interface ProductRepositoryInterface
{
    public function all();

    public function update($id, array $data);
}
