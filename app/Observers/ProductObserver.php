<?php

namespace App\Observers;

use App\Product;
use App\Repositories\ProductAvailabilityReminderRepositoryInterface;

class ProductObserver
{
    private $productAvailabilityReminderRepository;

    public function __construct(ProductAvailabilityReminderRepositoryInterface $productAvailabilityReminder)
    {
        $this->productAvailabilityReminderRepository = $productAvailabilityReminder;
    }


    /**
     * Handle the product "created" event.
     *
     * @param  \App\Product $product
     * @return void
     */
    public function created(Product $product)
    {
        //
    }

    /**
     * Handle the product "updated" event.
     *
     * @param  \App\Product $product
     * @return void
     */
    public function updated(Product $product)
    {
    }

    /**
     * Handle the product "deleted" event.
     *
     * @param  \App\Product $product
     * @return void
     */
    public function deleted(Product $product)
    {
        //
    }

    /**
     * Handle the product "restored" event.
     *
     * @param  \App\Product $product
     * @return void
     */
    public function restored(Product $product)
    {
        //
    }

    /**
     * Handle the product "force deleted" event.
     *
     * @param  \App\Product $product
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        //
    }

    public function updating(Product $product)
    {
        if ($product->quantity != $product->getOriginal('quantity') && $product->getOriginal('quantity') == 0) {
            $this->productAvailabilityReminderRepository->notify($product->sku);
        }
    }
}
