<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAvailabilityReminder extends Model
{
    protected $guarded = ['*'];

    protected $table = 'product_availability';

    public function setEmailAddress($email)
    {
        $this->email = $email;
    }

    public function setProductSku($productSku)
    {
        $this->product_sku = $productSku;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
    }
}
