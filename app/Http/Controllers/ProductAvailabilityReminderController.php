<?php

namespace App\Http\Controllers;

use App\Repositories\ProductAvailabilityReminderRepositoryInterface;
use Illuminate\Http\Request;

class ProductAvailabilityReminderController extends Controller
{

    private $productAvailabilityReminderRepository;

    public function __construct(ProductAvailabilityReminderRepositoryInterface $productAvailabilityReminder)
    {
        $this->middleware('auth');
        $this->productAvailabilityReminderRepository = $productAvailabilityReminder;
    }

    public function index()
    {
        return view('product_availability/index');
    }

    public function register(Request $request, $productSku)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'language' => 'required',
        ]);
        $email = $request->get('email');
        $language = $request->get('language');
        $result = $this->productAvailabilityReminderRepository->registerForProductAvailabilityReminder($email, $language, $productSku);
        if ($result['success']) {
            $request->session()->flash('status', 'Email added successfully');
        } else {
            $request->session()->flash('error', $result['error']);
        }
        return $this->index();
    }
}
