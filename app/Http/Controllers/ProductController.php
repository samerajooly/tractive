<?php

namespace App\Http\Controllers;

use App\Product;
use App\Repositories\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->middleware('auth');
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        $products = $this->productRepository->all();
        return view('products/index', compact('products'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'quantity' => 'required|numeric|min:0',
        ]);
        $quantity = $request->get('quantity');
        $result = $this->productRepository->update($id, ['quantity' => $quantity]);
        return response()->json($result);
    }

}
