<?php

namespace App\Mail;

use App\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductAvailableMail extends Mailable
{
    use Queueable, SerializesModels;

    private $product;
    private $language;

    /**
     * Create a new message instance.
     *
     * @param $product
     * @param $language
     */
    public function __construct($product, $language)
    {
        $this->product = $product;
        $this->language = $language;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        logger("HERE!!");
        $msg = "{$this->product->name} is now available!";
        if ($this->language == 'de') {
            $msg = "{$this->product->name} ist jetzt verfügbar!";
        }
        return $this->view('emails.productAvailable',compact('msg'));
    }
}
