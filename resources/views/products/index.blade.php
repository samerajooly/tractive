@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Products</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @for($i = 0;$i < sizeof($products);$i++)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <img width="100%" height="100%" src="/images/product{{$i + 1}}.png">
                                        </div>
                                        <div class="card-header">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <button type="button" class="btn btn-success updateQuantityBtn"
                                                            data-productid="{{$products[$i]->id}}"
                                                            data-toggle="modal" data-target="#updateQuantity"
                                                    >Update
                                                    </button>
                                                </div>
                                                <div class="col-md-4">
                                                    {{$products[$i]->name}} ({{$products[$i]->quantity}})
                                                </div>
                                                <div class="col-md-4">
                                                    @if($products[$i]->quantity ==0)
                                                        <button type="button" class="btn btn-warning"
                                                                onclick="window.location='{{ url("product/{$products[$i]->sku}/availability-reminder/register") }}'">
                                                            Notify
                                                        </button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(++$i < sizeof($products))
                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="card-body">
                                                <img width="100%" height="100%" src="/images/product{{$i + 1}}.png">
                                            </div>
                                            <div class="card-header">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <button type="button" class="btn btn-success updateQuantityBtn"
                                                                data-productid="{{$products[$i]->id}}"
                                                                data-toggle="modal" data-target="#updateQuantity"
                                                        >Update
                                                        </button>
                                                    </div>
                                                    <div class="col-md-4">
                                                        {{$products[$i]->name}} ({{$products[$i]->quantity}})
                                                    </div>
                                                    <div class="col-md-4">
                                                        @if($products[$i]->quantity ==0)
                                                            <button type="button" class="btn btn-warning"
                                                                    onclick="window.location='{{ url("product/{$products[$i]->sku}/availability-reminder/register") }}'">
                                                                Notify
                                                            </button>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="updateQuantity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Product Quantity</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="quantity">Quantity</label>
                        <input id="quantity" type="number" class="form-control">
                        <h8 id="quantityError" style="color:red"></h8>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary updateProductQuantity">Update</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            var selectedProductId = -1;
            $('.updateQuantityBtn').click(function (e) {
                e.preventDefault();
                selectedProductId = $(this).data('productid');
            });

            $('.updateProductQuantity').click(function (e) {
                $.ajax({
                        url: 'products/' + selectedProductId,
                        type: 'PUT',
                        data: {
                            quantity: $('#quantity').val(),
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function () {
                            location.reload();
                        },
                        error: function (error) {
                            $('#quantityError').text(error.responseJSON.errors.quantity);
                        }
                    }
                )
            });
        });
    </script>

@endsection
