@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Product Availability Reminder Registration
                        <button class="float-right btn btn-primary"
                                onclick="window.location='{{ url("products") }}'"
                        >Products
                        </button>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        @endif
                        {{ Form::open() }}
                        <div class="form-group">
                            <label for="email">Email address</label>
                            {{Form::text('email',null,[
                            'id'=>'email',
                            'class'=>'form-control',
                            'placeholder' =>'Enter Email',
                            ])}}
                            <h8 style="color:red"> {{$errors->first('email')}}</h8>
                        </div>
                        <div class="form-group">
                            <label for="language">Language</label>
                            {{Form::select('language',['en'=>'English','de'=>'German'],'en',['class'=>"form-control"])}}
                        </div>
                        <button type="submit" class="btn btn-success float-right">Register</button>
                        {{ Form::close() }}
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
