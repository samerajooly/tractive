<?php

use App\Product;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Product::query()->insert([
            [
                'id' => 1,
                'name' => 'Shirt',
                'sku' => 'white-shirt',
                'quantity' => 0
            ], [
                'id' => 2,
                'name' => 'Remote',
                'sku' => '123-remote',
                'quantity' => 0
            ]
        ]);
    }
}
